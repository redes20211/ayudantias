![UNAM](https://gitlab.com/ismael.andrade/redes-2021-1/-/raw/master/img/img_logoFC_2019.png)
# Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información




## Practica 1: Protocolo ARP y direcciones de capa de enlaces.

## Objetivo:
El alumno aprenderá a identificar direcciones lógicas y físicas de una interfaz de red.
Conocerá el funcionamiento del protocolo ARP mediante el uso de la herramienta Wireshark.

## Introducción
En las redes bajo los estándares IEEE802.3 y IEEE802.11, la forma de identificar físicamente a los
dispositivos de red es a través de la llamada dirección MAC (Media Access Control address).
También se le conoce como dirección física (physical address), dirección de hardware (hadware
address), dirección LAN (Local Área Network address).

Está compuesta por 48 bits los cuales se representan en seis grupos de dos dígitos hexadecimales.


## Desarrollo

### Video

El video se encuentra en la siguiente liga: https://www.youtube.com/watch?v=fkodiS86AfI

### Instalación de máquinas virtuales, virtualbox y vagrant:

1. Instalarás **Vagrant** que nos permitirá administrar las imágenes virtuales de manera más sencilla, sigue los pasos de instalación en esta liga: https://www.vagrantup.com/downloads.html
3. Clonar repositorio de la practica.
4. Ingresar a la carpeta del repositorio
5. Ingresar a la carpeta del laboratorio **lab1**

### Creación de la máquina virtual
1. Dentro de la carpeta **lab1/** (donde se encuentra el VagrantFile)

2. Ejecutar la máquina virtual:
		`vagrant up`
4. La máquina virtual del laboratorio se descargará e iniciará.
5. Ingresar a la consola de la máquina virtual con:
		`vagrant ssh`

	​	

## Desarrollo

1. Dentro de la **máquina virtual** ejecutar dentro de la terminal el comando para averiguar las direcciones físicas y lógicas:
`ip addr`
2. Dentro de la máquina física (tu sistema operativo), averiguar las direcciones física y lógica, esto dependerá del sistema operativo instalado, para windows es `ipconfig \all`

**Pregunta:**
1. **¿Cúales son las direcciones físicas de tu equipo y de la máquina virtual?**
2. **¿Cúales son las direcciones lógicas de tu equipo y la máquina virtual?**

### Wireshark
#### Instalación

1. Dentro de tu sistema operativo base instala wireshark del sitio : https://www.wireshark.org/#download
2. Inicia wireshark indicando que escuchará la interface de red **vboxnet** 
3. Puedes colocar un filtro colocando la palabra **arp**

#### Desarrollo

1. Ya que conoces las direcciones, realiza un comando `ping` de la máquina virtual a la máquina física de la siguiente manera, colocando la dirección lógica obtenida con el comando anterior (ip addr)
`ping <dirección lógica>`
2. Wireshark mostrará el tráfico ARP

## Evaluación

3. ¿Cuál es la diferencia a nivel de bits entre una dirección física y una lógica?
4. ¿Por qué existen dos consultas ARP?
5. Investigar y describir de manera breve en que consiste un ataque de ARP spoofing.
6. Investigar el fabricante del adaptador de red físico del equipo personal.

### Notas adicionales
1. El reporte se entrega de manera individual.
2. Registrar en el reporte los pasos que sean considerados necesarios para explicar cómo se realizó la práctica, incluir capturas de pantalla que justifiquen los resultados obtenidos. (Documento PDF)
3. Incluir las respuestas del Cuestionario en el reporte.
4. Se pueden agregar posibles errores, complicaciones, opiniones, críticas de la práctica o del laboratorio, o cualquier comentario relativo a la práctica.
5. Subir los archivos relacionados con la práctica dentro del Google drive de la clase > en la carpeta personalizada con su nombre > dentro de un directorio llamado Lab1
